package HillelHomeworkOneTaskOne;

public class RunApp {

    public static void main(String[] args) {

        Human personOne = new Human("Алексей", "Крайник");
        Human personTwo = new Human("Алексей", "Крайник", "Игоревич");

        System.out.println(personOne.getFullName());
        System.out.println(personOne.getShortName());
        System.out.println(personTwo.getFullName());
        System.out.println(personTwo.getShortName());
    }

}
