package HillelHomeworkOneTaskOne;

public class Human {

    private String personName;
    private String personSurname;
    private String personPatronymic;

    public Human(String personName, String personSurname) {
        this.personName = personName;
        this.personSurname = personSurname;
    }

    public Human(String personName, String personSurname, String personPatronymic) {
        this.personName = personName;
        this.personSurname = personSurname;
        this.personPatronymic = personPatronymic;
    }

    public String getFullName() {

        if (personPatronymic == null) {
            return personSurname + " " + personName;
        } else {
            return personSurname + " " + personName + " " + personPatronymic;
        }
    }

    public String getShortName() {

        if (personPatronymic == null) {
            return personSurname + " " + personName.charAt(0) + ".";
        } else {
            return personSurname + " " + personName.charAt(0) + "." + personPatronymic.charAt(0) + ".";
        }
    }

}
